package useragent;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;

public class MasterSender extends Thread {

	private Socket TCPSocket;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private BufferedReader myinput;
    private BasicTextEncryptor textEncryptor;
    private String username;
    private String SERVER_IP;
    private int SERVER_PORT;
	String GROUP_IP;
	int PORT;
	String keyMex = "";
	MulticastSocket socket;
	
	public MasterSender(MulticastSocket socket, Socket TCPSocket, PrintWriter out, BufferedReader in, BufferedReader myinput, String username, String SERVER_IP, int SERVER_PORT){
		this.socket = socket;
		this.TCPSocket = TCPSocket;
		this.out = out;
		this.in = in;
		this.myinput = myinput;
		this.username = username;
		this.SERVER_IP = SERVER_IP;
		this.SERVER_PORT = SERVER_PORT;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void run(){
		String comm="";
		boolean end = false;
		Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }
		while(true){
			c.format("Digitare quit (q) per interrompere la partita%n");
			try {
				while ((!myinput.ready())&&(!Thread.interrupted())) {
					Thread.sleep(200);
			    }
				if(!Thread.interrupted())
					comm = myinput.readLine();
			} catch (IOException | InterruptedException e) {
				end = true;
			}
			if(comm.equals("q")||(comm.equals("quit"))||(end)){
				if((TCPSocket!=null)&&(!TCPSocket.isClosed())){
					try {		//Chiusura TCP
						out.close();
						in.close();
						TCPSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(socket!=null){
                    try {
    					JSONObject mexToGuesser = new JSONObject();
                		mexToGuesser.put("TYPE", "INTERRUPT");
                		textEncryptor = new BasicTextEncryptor();
                		textEncryptor.setPassword(keyMex);
						send(mexToGuesser.toJSONString());	//Chiusura multicast, mando interruzione ai guesser
					} catch (IOException e) {
						System.exit(-1);
					}
					socket.close();
				}
				
				try {
					TCPSocket = new Socket(SERVER_IP, SERVER_PORT); //Al termine comunico alla registry che la partita è conclusa
					out = new PrintWriter(TCPSocket.getOutputStream(), true);
					in = new BufferedReader(new InputStreamReader(TCPSocket.getInputStream()));
				} catch (UnknownHostException e) {
					System.err.println("Host Sconosciuto.");
					Thread.currentThread().stop();
				} catch (IOException e) {
					System.err.println("Registry non pronta");
					Thread.currentThread().stop();
				}
				JSONObject mex = new JSONObject();
				mex.put("TYPE", "DELETE");
				mex.put("MASTER", username);

				out.println(mex);
				
				break;
			}
		}
	}
	
	private void send(String dString) throws UnknownHostException, IOException {
		byte[] buf;
		buf = (textEncryptor.encrypt(dString)).getBytes();
		InetAddress group = InetAddress.getByName(GROUP_IP);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, group, PORT);
		socket.send(packet);
	}
}
