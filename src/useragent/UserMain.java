package useragent;

import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.NameNotFoundException;
import javax.naming.NoPermissionException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import registry.RegistryIF;

public class UserMain extends UnicastRemoteObject implements UserIF {

	private static final long serialVersionUID = 1L;
	private static int SERVER_PORT;
	private static String LOCAL_IP;
	private static String SERVER_IP;
	private static UserMain us;
	
	protected UserMain() throws RemoteException {}

	public static void main(String[] args) throws IOException{
		String username=""; //nome utente corrente
		String password=""; //password utente corrente
		String mode="";
		String choose;
		boolean loop=true;
		JSONObject obj=null;
		
		Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }
		
		try{
			username=args[0];
		}
		catch (Exception e){
			System.err.println("Inserimento scorretto");
			System.exit(-1); 
		}
		
		try {
			obj = (JSONObject) new JSONParser().parse(new FileReader("MasterConfig.json"));
			LOCAL_IP = (String) obj.get("LOCAL_IP");
			SERVER_IP = (String) obj.get("SERVER_IP");
			SERVER_PORT = Integer.parseInt((String) obj.get("SERVER_PORT"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.setProperty("java.rmi.server.hostname", LOCAL_IP);
		RegistryIF name = null;
		try {
			name = (RegistryIF) Naming.lookup("rmi://"+SERVER_IP+":1111/MyServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.err.println ("Errore connessione Server RMI");
			System.exit(-1); 
		}
		while(true){
		 while(loop){
			c.format("Che operazione vuoi fare %s?%n'l' : Login , 'r' : Registrazione , 'q' : Quit%n", username);
			choose = c.readLine();
			switch (choose){
			case "l":
				c.format("Login: Utente %s inserire password%n",username);
				password = c.readLine();
				us = new UserMain();
				try {
					name.login(username, password, us);
					loop=false;
					c.format("Login effettuato!%n"); 
				} catch (DuplicateName e) {
					c.format("L'utente a questo nome risulta già connesso%n");
				} catch (NoPermissionException e) {
					c.format("Password Errata%n");
				} catch (NameNotFoundException e) {
					c.format("Utente non registrato%n");
				}
				break;
			case "r":
				c.format("Registrazione: Utente %s inserire password%n",username);
				password = c.readLine();
				try {
					name.signin(username, password);
					c.format("Utente registrato%n");
				} catch (DuplicateName e) {
					c.format("Nome Utente già registrato%n"); 
				}
				break;
			case "q":
				c.format("Arrivederci %s!%n",username);
				System.exit(1);
			}
		 }
		
		 //visualizzo partite in corso....
		
		
		 loop = true;
		 while (loop){
			c.format("In che modalità vuoi giocare? ('g' per Guesser, 'm' per Master, 'l' per Logout)%n");
			mode = c.readLine();
			switch(mode){
			case "m":
				MasterThread m = new MasterThread(username, LOCAL_IP, SERVER_IP, SERVER_PORT);
				m.start();
				try {
					m.join();
					Thread.sleep(200);
				} catch (InterruptedException e) {
					c.format("Master Interrotto%n");
				}
				break;
			case "g":
				GuesserThread g = new GuesserThread(username, LOCAL_IP, SERVER_IP, SERVER_PORT);
				g.start();
				try {
					g.join();
					Thread.sleep(200);
				} catch (InterruptedException e) {
					c.format("Guesser Interrotto%n");
				}
				break;
			case "l":
				name.logout(username, us);
				c.format("Utente Sconnesso%n");
				loop=false;
				break;
			}
		}
		loop = true;
	   }
	}

	@Override
	public void update(String mex) throws RemoteException {
		System.out.println(mex);
	}
}
