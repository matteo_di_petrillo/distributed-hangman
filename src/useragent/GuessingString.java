package useragent;

public class GuessingString {
	
	private String sequen = "";  //parola completa
	private String sequenCript = ""; //lettere scoperte
	private int guessed = 0;   //numero di lettere indovinate
	
	public GuessingString(String s){
		sequen=s;
		for(int i=0;i<sequen.length();i++){
			sequenCript=sequenCript + "-";
		}
	}
	public boolean isThere(char c){    //c'è la lettera?
		for(int i=0;i<sequen.length();i++){
			if(sequen.charAt(i)==c) return true;
		}
		return false;
	}
	
	public void trychar(char c){      //prova lettera
		String Temp = "";
		for(int i=0;i<sequen.length();i++){
			if(sequen.charAt(i) == c){  //lettera presente
				Temp = Temp + sequen.charAt(i);
				if(sequenCript.charAt(i) == '-'){ //lettera non ancora detta
					guessed++;
				}
			}
			else
				Temp = Temp + sequenCript.charAt(i);
		}
		sequenCript=Temp;
	}
	
	public boolean isFinished(){  //indovinate tutte?
		if(guessed == sequen.length())
			return true;
		return false;
	}
	
	public String show(){
		return sequenCript;
	}
}
