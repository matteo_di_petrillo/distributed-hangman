package useragent;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;

public class GuesserSender extends Thread {
	
	private Socket TCPSocket;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private String username;
	private BufferedReader myinput;
    private static BasicTextEncryptor textEncryptor;
	String MASTER_IP;
	int PORT;
	String keyMex="";
	MulticastSocket socket;
	boolean mustResend = false ;
	
	public GuesserSender(String username, BufferedReader myinput,  Socket TCPSocket, PrintWriter out, BufferedReader in){

		this.username=username;
		this.myinput=myinput;
		this.TCPSocket = TCPSocket;
		this.out = out;
		this.in = in;
	}
	
	@SuppressWarnings("unchecked")
	public void run(){
		JSONObject mexToMaster;
		String letter="";
		boolean end = false;
		Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }

		c.format("Digitare 'quit' per uscire dalla partita%n");
		while (true){
			try {
		        while ((!myinput.ready())&&(!Thread.interrupted())&&(!mustResend)) {
		          Thread.sleep(200);
		        }
		        if((!Thread.interrupted())&&(!mustResend))
		        	letter=myinput.readLine();
			} catch (IOException | InterruptedException e) {
				end = true;
			}
			if((letter.equals("quit"))||(end)||(Thread.interrupted())){
				c.format("Gioco finito, ritorno alla modalità di scelta...%n");
				if((TCPSocket!=null)&&(!TCPSocket.isClosed())){
					try {			//Chiudo connessione TCP se aperta
						out.close();
						in.close();
						TCPSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				if(socket!=null){	//Chiusura multicast
					socket.close();
				}
				
				break;
			}
			
			if(letter.length()>1){
				c.format("Inserire una sola lettera o 'quit' per uscire%n");
			}
			else{
				mexToMaster = new JSONObject();
				mexToMaster.put("TRY", letter);
				mexToMaster.put("USER", username);
				textEncryptor = new BasicTextEncryptor();
				textEncryptor.setPassword(keyMex);
				mustResend = false;
				try {
					send(mexToMaster.toJSONString());
				} catch (IOException e) {
					System.err.println("Connessione chiusa");
					break;
				}
			}
		}
	}
	private void send(String dString) throws UnknownHostException, IOException {
		byte[] buf;
		buf = (textEncryptor.encrypt(dString)).getBytes();
        InetAddress address = InetAddress.getByName(MASTER_IP);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, PORT);
		socket.send(packet);
		socket.setSoTimeout( 3000 );
	}
}
