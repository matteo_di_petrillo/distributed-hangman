package useragent;

import java.io.*;
import java.net.*;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MasterThread extends Thread {

	private static int PORT;
	private int SERVER_PORT;
	private String LOCAL_IP;
	private String SERVER_IP;
	private static String GROUP_IP;
	protected MulticastSocket socket = null;
    protected boolean loop = true;
    private String username;
    private String keyMex="";
    private static BasicTextEncryptor textEncryptor;

	Socket TCPSocket = null;
	PrintWriter out = null;
	BufferedReader in = null;
	
    String word="";
    int nPenalty=0;
    int nGuesser=0;


    public MasterThread(String username, String LOCAL_IP, String SERVER_IP, int SERVER_PORT) throws IOException {

		this.username=username;
		this.LOCAL_IP = LOCAL_IP;
		this.SERVER_IP = SERVER_IP;
		this.SERVER_PORT = SERVER_PORT;
	}

    @SuppressWarnings({ "unchecked", "deprecation" })
	public void run() {
    	
		JSONObject mex = new JSONObject();
		JSONObject obj=null;
		JSONObject mexToGuesser = new JSONObject();
		String resp;
		String letter="";
		String userGuesser="";
		String type;
		MasterSender m = null;
    	
    	InputStreamReader reader = new InputStreamReader(System.in);
		BufferedReader myinput = new BufferedReader(reader);
		
		Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }
        c.format("Benvenuto Master %s, Inserisci Parola%n",username);
		try {
			word=myinput.readLine();
		} catch (IOException e) {
			System.err.println ("Errore inserimento: " + e);
			Thread.currentThread().stop();
		}
        GuessingString Word = new GuessingString(word);
        c.format("Quanti Guesser partecipanti desideri alla partita?%n");
		try {
			nGuesser=Integer.parseInt(myinput.readLine());
		} catch (Exception e) {
			System.err.println ("Inserimento numerico scorretto");
			Thread.currentThread().stop();
		}
		
		//Connessione TCP alla Registry
		try {
			TCPSocket = new Socket(SERVER_IP, SERVER_PORT);
			out = new PrintWriter(TCPSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(TCPSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Host Sconosciuto.");
			Thread.currentThread().stop();
		} catch (IOException e) {
			System.err.println("Registry non pronta");
			Thread.currentThread().stop();
		}
		try {
			mex.put("TYPE", "CREATE");
			mex.put("USER", username);
			mex.put("NGUESSER", ""+nGuesser);
			mex.put("MASTER_IP", LOCAL_IP);
			out.println(mex);
			
			m = (new MasterSender(socket,TCPSocket,out,in,myinput,username,SERVER_IP,SERVER_PORT));
			m.start();
			resp=in.readLine();
			if(resp==null){ //In caso di arresto della registry
    			System.err.println("Registry non attiva");
    			m.interrupt();
    			TCPclose();
    			Thread.currentThread().stop();
            }
            obj = (JSONObject) new JSONParser().parse(resp);
            String ty = (String) obj.get("TYPE");
            if((ty==null)||ty.equals("INTERRUPTED")){
    			System.err.println("Interruzione partita, TimeOut scaduto");
    			m.interrupt();
    			TCPclose();
    			Thread.currentThread().stop();
            }
            if(ty.equals("FULL_GAMES")){
            	System.err.println("Partite momentaneamente al completo, riprova piu' tardi");
    			m.interrupt();
    			TCPclose();
    			Thread.currentThread().stop();
            }
			PORT = Integer.parseInt((String) obj.get("PORT"));
			GROUP_IP = (String) obj.get("GROUP_IP");
			keyMex = (String) obj.get("KEY_MEX");
			m.keyMex=keyMex;
			socket = new MulticastSocket(PORT);
			socket.setInterface(InetAddress.getByName(LOCAL_IP));
			Thread.sleep(200);
			TCPclose();
			m.PORT=PORT;
			m.GROUP_IP=GROUP_IP;
			m.socket=socket;
		} catch (IOException | ParseException | InterruptedException e) {
			System.err.println("Interruzione partita");
			m.interrupt();
			Thread.currentThread().stop();
		}

        c.format("Inizio partita%n");
		//Inizio Partita
		
		textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(keyMex);

		type="INIT";
        while (loop) {
            try {
    			Thread.sleep(200);
        		mexToGuesser.put("TYPE", type);
        		mexToGuesser.put("LETTER", letter);
        		mexToGuesser.put("WORD", Word.show());
        		mexToGuesser.put("NTRY", ""+nPenalty);
        		mexToGuesser.put("USERG", userGuesser);
                send(mexToGuesser.toJSONString());
                c.format("%s%n",Word.show());
    			String received = receive(socket);
                obj = (JSONObject) new JSONParser().parse(received);
        		letter = (String) obj.get("TRY");
        		userGuesser = (String) obj.get("USER");
                if(letter.length()==1){ //controllo solo se è un carattere
                	Word.trychar(letter.charAt(0));
                	if(Word.isThere(letter.charAt(0))){
                		type="PLAY";
                	}else{
                		type="PLAYBAD";
                		nPenalty++;
                	}
                }
                c.format("%s ha proposto la lettera %s . Penalità attuali: %d%n",userGuesser,letter,nPenalty);
		        if(nPenalty==10){
		        	loop=false;
	                c.format("%s%nI Guesser hanno perso, tentativi esauriti%n",Word.show());
                	type="FINISHBAD";
            		mexToGuesser.put("TYPE", type);
            		mexToGuesser.put("WORD", Word.show());
            		mexToGuesser.put("USERG", userGuesser);
            		mexToGuesser.put("LETTER", letter);
                    send(mexToGuesser.toJSONString());
		        }
                if(Word.isFinished()){
                	loop=false;
	                c.format("%s%nI Guesser hanno vinto, parola complatata%n",Word.show());
                	type="FINISH";
            		mexToGuesser.put("TYPE", type);
            		mexToGuesser.put("WORD", Word.show());
            		mexToGuesser.put("USERG", userGuesser);
            		mexToGuesser.put("LETTER", letter);
                    send(mexToGuesser.toJSONString());
                }
            }
            catch (IOException e) {
    			System.err.println("Interruzione partita");
    			m.interrupt();
    			Thread.currentThread().stop();;
            } catch (ParseException e) {
            	m.interrupt();
    			Thread.currentThread().stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
    			Thread.currentThread().stop();
			}
        }
        m.interrupt();
    }
	private void send(String dString) throws UnknownHostException, IOException {
		byte[] buf;
		buf = (textEncryptor.encrypt(dString)).getBytes();
		InetAddress group = InetAddress.getByName(GROUP_IP);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, group, PORT);
		socket.send(packet);
	}
	private static String receive(MulticastSocket socket) throws IOException {
		DatagramPacket packet;
		byte[] buf = new byte[256];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            String received = new String(packet.getData(), 0, packet.getLength());
            received = textEncryptor.decrypt(received);
		return received;
	}
	private void TCPclose() throws IOException{
		out.close();
		in.close();
		TCPSocket.close();
	}
}
