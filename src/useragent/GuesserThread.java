package useragent;

import java.io.*;
import java.net.*;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GuesserThread extends Thread{
	
	private static int PORT;
	private int SERVER_PORT;
	private String LOCAL_IP;
	private String SERVER_IP;
	private static String MASTER_IP;
	private static String GROUP_IP;
	MulticastSocket socket;
    protected boolean loop = false;
	private String username;
	private String userMaster;
	private String keyMex = "";
    private static BasicTextEncryptor textEncryptor;
	
	Socket TCPSocket = null;
	PrintWriter out = null;
	BufferedReader in = null;

	public GuesserThread(String username, String LOCAL_IP, String SERVER_IP, int SERVER_PORT) throws IOException {
		
		this.username = username;
		this.LOCAL_IP = LOCAL_IP;
		this.SERVER_IP = SERVER_IP;
		this.SERVER_PORT = SERVER_PORT;
		
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void run(){
		
		JSONObject mex = new JSONObject();
		JSONObject obj=null;
		String resp;
		String type;
		String word;
		String nPenalty;
		String userG;
		String lett;
		boolean stop = false;
		GuesserSender g = null;
		
        InputStreamReader reader = new InputStreamReader(System.in);
		BufferedReader myinput = new BufferedReader(reader);
		Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }
        
		try {
			TCPSocket = new Socket(SERVER_IP, SERVER_PORT);
			out = new PrintWriter(TCPSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(TCPSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Host Sconosciuto");
			Thread.currentThread().stop();
		} catch (IOException e) {
			System.err.println("Registry non pronta");
			Thread.currentThread().stop();
		}
		c.format("A quale Master vuoi collegarti?%n");
		try {
			userMaster=myinput.readLine();
		} catch (IOException e) {
			System.err.println ("Errore inserimento: " + e);
			Thread.currentThread().stop();
		}
		c.format("Attendere prego...%n");
		try {
			mex.put("TYPE", "ACCEPT");
			mex.put("USERGUESSER", username);
			mex.put("USERMASTER", userMaster);
			out.println(mex);
				
			g = new GuesserSender(username, myinput,TCPSocket,out,in);
			g.start();
		
			resp=in.readLine();
			if(resp==null){ //In caso di arresto della registry
    			System.err.println("Registry non attiva");
    			g.interrupt();
	    		TCPclose();
	    		Thread.currentThread().stop();
	        }
			
			obj = (JSONObject) new JSONParser().parse(resp);
			String typ = (String) obj.get("TYPE");
			switch (typ) {
				case "OK":
					PORT = Integer.parseInt((String) obj.get("PORT"));
					GROUP_IP = (String) obj.get("GROUP_IP");
					MASTER_IP = (String) obj.get("MASTER_IP");
					keyMex = (String) obj.get("KEY_MEX");
					textEncryptor = new BasicTextEncryptor();
					textEncryptor.setPassword(keyMex);
					socket = joins();

					g.keyMex = keyMex;
					g.PORT = PORT;
					g.MASTER_IP = MASTER_IP;
					g.socket = socket;

					break;
				case "NO_NAME":
					System.err.println("Nome Master non presente");
		   			g.interrupt();
					stop=true;
					break;
				case "TOO_LATE":
					System.err.println("Partita già cominciata");
		   			g.interrupt();
					stop=true;
					break;
				case "ESCAPED":
					System.err.println("Il Master ha abbandonato la partita");
		   			g.interrupt();
					stop=true;
					break;
				case "INTERRUPTED":
					System.err.println("TimeOut Scaduto, chiusura partita");
		    		g.interrupt();
					stop=true;
					break;
			}

			TCPclose();
			
		} catch (IOException | ParseException e) {

			System.err.println("Interruzione partita");
			g.interrupt();
			Thread.currentThread().stop();
		}
		if(!stop)
			loop=true;
		else
			g.interrupt();
		try{
		 while (loop) {
			 
			boolean loop2 = true;
			int trys = 0;
			
			while(loop2){	//controllo se l'ack del master arriva entro il timeout
				try{
					String received = receive(socket);	
					loop2=false;
					obj = (JSONObject) new JSONParser().parse(received);
					g.socket.setSoTimeout(0);
				}catch (SocketTimeoutException e) {
					g.mustResend = true;
					trys ++;
					if(trys > 9){
						System.err.println("Tentativi di rinvio esauriti");
						g.interrupt();
						Thread.currentThread().stop();
					}
				}
			}
			
			type = (String) obj.get("TYPE");
			word = (String) obj.get("WORD");
			nPenalty = (String) obj.get("NTRY");
			userG = (String) obj.get("USERG");
			lett = (String) obj.get("LETTER");
			if (type!=null){
			 switch (type) {
				case "INIT":	
					c.format("Inizia la partita!%n");
					break;
				case "PLAY":
					c.format("%s ha proposto la lettera %s%n",userG,lett);
					break;
				case "PLAYBAD":
					c.format("%s ha proposto la lettera %s ma non è presente%n",userG,lett);
					break;
				case "FINISH":
					c.format("%s ha proposto la lettera %s e ha concluso la parola!%nParola completata: %s HAI VINTO!!%n",userG,lett,word);
					loop=false;
					break;
				case "FINISHBAD":
					c.format("%s ha proposto la lettera %s terminando i tentativi%n--Hai perso!!",userG,lett);
					loop=false;
					break;
				case "INTERRUPT":
					System.err.println("Partita Interrotta!");
					loop=false;
					break;
			 }
			 if(loop==true)
				 c.format("Parola corrente: %s ; Numero di tentativi: %s ; Inserire lettera%n",word,nPenalty);
			}
		 }
		 if(!stop){
			 socket.leaveGroup(InetAddress.getByName(GROUP_IP));
			 socket.close();
			 g.interrupt();
		 }
		}
		
		catch (IOException e) {
            System.err.println("Interruzione partita");
	        loop = false;
        } catch (ParseException e1) {
			e1.printStackTrace();
		}
    }

	private static String receive(MulticastSocket socket) throws IOException {
		DatagramPacket packet;
		byte[] buf = new byte[256];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            String received = new String(packet.getData(), 0, packet.getLength());
            received = textEncryptor.decrypt(received);
		return received;
	}

	private MulticastSocket joins() throws UnknownHostException,
			IOException, SocketException {
		    	InetAddress iface = InetAddress.getByName(LOCAL_IP);
		    	MulticastSocket socket = new MulticastSocket(PORT);
		        InetAddress address = InetAddress.getByName(GROUP_IP);
		        socket.setInterface(iface);
		        socket.joinGroup(address);
		return socket;
	}
	
	private void TCPclose() throws IOException{
		out.close();
		in.close();
		TCPSocket.close();
	}
}
