package registry;

import java.rmi.Remote;
import java.rmi.RemoteException;
import javax.naming.NameNotFoundException;
import javax.naming.NoPermissionException;

import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

public interface RegistryIF extends Remote{
	public void signin(String name, String password)throws DuplicateName, RemoteException;
	public void login(String name, String password, Object callback)throws NameNotFoundException, NoPermissionException, DuplicateName, RemoteException;
	public void logout(String name, Object callback)throws RemoteException;
}
