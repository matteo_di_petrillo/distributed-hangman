package registry;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RegistryCatcher extends Thread {

	private ConcurrentHashMap<String, GamesData> games;
	private String userMaster;
	private Socket socket;
	private String mode;
	
	
	public RegistryCatcher(ConcurrentHashMap<String, GamesData> games, String userMaster, Socket socket, String mode){

		this.games = games;
		this.userMaster = userMaster;
		this.socket = socket;
		this.mode = mode;
	}

	public void run() {
		
		final Runnable RegistryListner = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(300);
					if(socket.getInputStream().read()==-1){	// Il master ha chiuso la connessione
						if (mode.equals("M")) //Se master interrompe, annulla la partita
							games.get(userMaster).setStatus("INVALIDATE");
						if (mode.equals("G")) //Se guesser interrompe, decresce il numero guesser
							games.get(userMaster).subCurrentGuessers();
					}		
				} catch (IOException | InterruptedException e) {}	// il server ha abilitato la partita e chiuso la connessione
			}
		};
		
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		final Future<?> future = executor.submit(RegistryListner);
		executor.shutdown();
		
		try { 
			future.get(1, TimeUnit.MINUTES); 
		}
		catch (InterruptedException e) {} //thread interrotto
		catch (ExecutionException e) {} //eccezione esecuzione
		catch (TimeoutException e) { 
			games.get(userMaster).setStatus("INTERRUPTED"); //Timeout lungo scaduto
		}
		if (!executor.isTerminated())
		    executor.shutdownNow();
	}
}
