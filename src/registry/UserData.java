package registry;

public class UserData {
	private String password;
	private String status;
	
	public UserData(String password, String status){
		this.setPassword(password);
		this.setStatus(status);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
