package registry;

import java.io.*;
import java.net.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import useragent.UserIF;

public class RegistryServerThread extends Thread {
    private Socket socket = null;
    private String type;
    private String userGuesser;
    private String userMaster;
    private int nGuesser;
    private boolean num[];
    private ConcurrentHashMap<String, GamesData> games;
	private int PORT;
	private String GROUP_IP;
	private int MAX_GAMES;
	private LinkedList <Object> callbacks;
    
    public RegistryServerThread(Socket socket, ConcurrentHashMap<String, GamesData> games, boolean[] num, int MAX_GAMES, String GROUP_IP, int PORT, LinkedList <Object> callbacks) {
    	this.socket = socket;
    	this.games = games;
    	this.num = num;
    	this.MAX_GAMES = MAX_GAMES;
    	this.GROUP_IP = GROUP_IP;
    	this.PORT = PORT;
    	this.callbacks = callbacks;
    }
    @SuppressWarnings("unchecked")
	public void run() {

    	JSONObject mexToUser = new JSONObject();
    	JSONObject obj=null;
    	int ind;
    	boolean flag = true;
    	Console c = System.console();
        if (c == null) {
            System.err.println("Nessuna console.");
            System.exit(1);
        }
    	
    	try {
    		String mex;
    		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
    		BufferedReader in = new BufferedReader(
    				new InputStreamReader(
    						socket.getInputStream()));
    		mex=in.readLine();
    		obj = (JSONObject) new JSONParser().parse(mex);
    		type = (String) obj.get("TYPE");
    		
    		if(type.equals("CREATE")){
    			
        	  for(ind=0; ind<MAX_GAMES; ind++){
        		if(num[ind]){
        			break;
        		}
        	  }
        	  if(ind == MAX_GAMES){ //Limite max partite raggiunto 
            		mexToUser.put("TYPE", "FULL_GAMES");
            		flag=false;
        	  }
        	  else{
				num[ind]=false;
    			userMaster = (String) obj.get("USER");
    			nGuesser = Integer.parseInt((String) obj.get("NGUESSER"));
    			String MASTER_IP = (String) obj.get("MASTER_IP");
    			
    			long keyMex = (long) (Math.random() * 10000);	//chiave di cifratura casuale
    			
    			GamesData info= new GamesData(nGuesser, MASTER_IP, ind, ""+keyMex);
    			games.put(userMaster, info);
    			c.format("Master %s aspetta %s guesser%n",userMaster, nGuesser);
    			new RegistryCatcher(games,userMaster,socket,"M").start();
    			
        		int i=0;
    			while(i < callbacks.size()){
    				((UserIF) callbacks.get(i)).update("** " +userMaster + " ha creato una partita ed è in attesa di " + nGuesser + " guessers\r\n" + statusGames());
    				i++;
    			}
    			
    			while(((games.get(userMaster).getStatus()).equals("OK")) && (games.get(userMaster).reachGuessers()));
    			
    			if(games.get(userMaster).reachGuessers()){	//Se il master ha chiuso la partita
    	    	    flag=false;
    			}else{
        			c.format("Master %s inizia partita con %s guesser%n",userMaster, nGuesser);
    				games.get(userMaster).setBegun(true);
    			}
        	  }
    		}
    		
    		if(type.equals("ACCEPT")){
    			
    			userGuesser = (String) obj.get("USERGUESSER");
    			userMaster = (String) obj.get("USERMASTER");
    			if (games.containsKey(userMaster)){
    			  if(!games.get(userMaster).isBegun()){ //Se la partita non è gia cominciata
    				games.get(userMaster).addCurrentGuessers();
    				new RegistryCatcher(games,userMaster,socket,"G").start();
    				c.format("Guesser %s gioca con %s%n",userGuesser, userMaster);    				
    	    		int i=0;
    				while(i < callbacks.size()){
    					((UserIF) callbacks.get(i)).update("** " +userGuesser + " partecipa alla partita di " + userMaster + "\r\n" + statusGames());
    					i++;
    				}
    				
    				while(((games.get(userMaster).getStatus()).equals("OK")) && (games.get(userMaster).reachGuessers()));
    				
    				if((games.get(userMaster).getStatus()).equals("INVALIDATE")){//Il Master ha abbandonato la partita
                		mexToUser.put("TYPE", "ESCAPED"); 
    					flag=false;
    				}
    				if((games.get(userMaster).getStatus()).equals("INTERRUPTED")){//TimeOut Scaduto
                		mexToUser.put("TYPE", "INTERRUPTED");
    					flag=false;
    				}
    			  }
    			  else{
              		mexToUser.put("TYPE", "TOO_LATE");	//La partita ha già raggiunto i guesser ed è in corso
              		flag=false;
    			  }
    			}
    			else{
            		mexToUser.put("TYPE", "NO_NAME");	//Il nome del Master della partita non è presente
            		flag=false;
    			}
    		}
    		
    		if(type.equals("DELETE")){
    			userMaster = (String) obj.get("MASTER");
	    	    flag=false;
    			if(games.containsKey(userMaster)){
    				num[games.get(userMaster).getInd()]=true;	//Libero l'indirizzo per un altra partita
    	    		try {
    	    			Thread.sleep(500);
    	    		} catch (InterruptedException e) {}
    				games.remove(userMaster);
    				
    				int i=0;
    				while(i < callbacks.size()){
    					((UserIF) callbacks.get(i)).update("** La partita di " + userMaster + " e' terminata\r\n" + statusGames());
    					i++;
    				}
    			}
    		}
    		
    		if (flag){
    			String lastInd = GROUP_IP.substring(GROUP_IP.lastIndexOf(".") + 1);
    			String firstInd = GROUP_IP.substring(0, GROUP_IP.lastIndexOf(".")+1); //Parte fissa indirizzo
    			ind = games.get(userMaster).getInd();
    			mexToUser.put("TYPE", "OK");
    			mexToUser.put("PORT", (PORT + ind) +"");
    			mexToUser.put("GROUP_IP", firstInd + (Integer.parseInt(lastInd) + ind));
    			mexToUser.put("MASTER_IP", games.get(userMaster).getMASTER_IP());
    			mexToUser.put("KEY_MEX", games.get(userMaster).getKeyMex());
    		}
    		out.println(mexToUser.toJSONString());
    		out.close();
    	    in.close();
    	    socket.close();

    	} catch (IOException e) {
    	    e.printStackTrace();
    	} catch (ParseException e) {
			e.printStackTrace();
		}
    }
    
    public String statusGames(){
		Iterator<String> iterator = games.keySet().iterator();
		String ris = "";  
		while (iterator.hasNext()) {
		   String key = iterator.next().toString();
		   String value = games.get(key).stampGames();
		   ris = ris +  " *** " + (key + " " + value) + "\r\n";
		}
		if (ris.equals(""))
			return " *** Nessuna partita aperta al momento";
		return ris;
	}
}
