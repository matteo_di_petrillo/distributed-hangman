package registry;

public class GamesData {
	private int nGuesser;
	private int currentGuessers;
	private String MASTER_IP;
	private int ind;
	private String Status;
	private boolean begun;
	private String keyMex;
	
	public GamesData(int nGuesser, String MASTER_IP, int ind, String keyMex){
		this.nGuesser=nGuesser;
		this.MASTER_IP = MASTER_IP;
		this.ind = ind;
		this.keyMex = keyMex;
		currentGuessers=0;
		Status = "OK";
		setBegun(false);
	}

	public int getnGuesser() {
		return nGuesser;
	}

	public int getCurrentGuessers() {
		return currentGuessers;
	}
	
	public synchronized void addCurrentGuessers() {
		currentGuessers++;
	}
	
	public synchronized void subCurrentGuessers() {
		currentGuessers--;
	}
	
	public synchronized boolean reachGuessers() {
		return (currentGuessers < nGuesser);
	}

	public String getMASTER_IP() {
		return MASTER_IP;
	}
	
	public int getInd() {
		return ind;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	public boolean isBegun() {
		return begun;
	}

	public void setBegun(boolean begun) {
		this.begun = begun;
	}
	public String getKeyMex() {
		return keyMex;
	}
	
	public String stampGames() {
		if(isBegun())
			return "ha gia raggiunto la quota di " + nGuesser + " guesser e sta giocando";
		return "Attende "+ (nGuesser - currentGuessers) +" guessers. Stato attuale: " + currentGuessers + "/" + nGuesser;
	}
}
