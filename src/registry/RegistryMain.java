package registry;

import java.io.FileReader;
import java.io.IOException;
import java.net.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.naming.NameNotFoundException;
import javax.naming.NoPermissionException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import useragent.UserIF;

public class RegistryMain extends UnicastRemoteObject implements RegistryIF {
	
	private static boolean num[];
	private static final long serialVersionUID = 1L;
	private static int SERVER_PORT;
	private static int MAX_GAMES;
	private static int FIRST_GAMEPORT;
	private static String FIRST_GROUP_IP;
	private static String LOCAL_IP;
	private static ConcurrentHashMap<String, GamesData> Games = new ConcurrentHashMap<String, GamesData>();
	private static ConcurrentHashMap<String, UserData> Users = new ConcurrentHashMap<String, UserData>();
	private static LinkedList <Object> callbacks = new LinkedList <Object>();
	
	public RegistryMain() throws RemoteException{}

	public static void main(String[] args) throws IOException, RemoteException {
		ServerSocket serverSocket = null;

		JSONObject obj;
		try {
			obj = (JSONObject) new JSONParser().parse(new FileReader("RegisterConfig.json"));
			LOCAL_IP = (String) obj.get("LOCAL_IP");
			SERVER_PORT = Integer.parseInt((String) obj.get("SERVER_PORT"));
			MAX_GAMES = Integer.parseInt((String) obj.get("MAX_GAMES"));
			FIRST_GAMEPORT = Integer.parseInt((String) obj.get("FIRST_GAMEPORT"));
			FIRST_GROUP_IP = (String) obj.get("FIRST_GROUP_IP");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		System.setProperty("java.rmi.server.hostname", LOCAL_IP);
		RegistryMain objServer = new RegistryMain();
		Registry reg = LocateRegistry.createRegistry(1111);
		reg.rebind("MyServer", objServer);
		
        try {
            serverSocket = new ServerSocket(SERVER_PORT);
        } catch (IOException e) {
            System.err.println("Non ricevo niente dalla porta "+ SERVER_PORT);
            System.exit(-1);
        }
        num = new boolean[MAX_GAMES];
        for(int i=0; i<MAX_GAMES; i++)
        	num[i]=true;
        Executor myPool = Executors.newFixedThreadPool(50);
		System.out.println("Server Pronto");
        try {
        	while ( true ) {
        		myPool.execute(new RegistryServerThread(serverSocket.accept(), Games, num, MAX_GAMES, FIRST_GROUP_IP, FIRST_GAMEPORT, callbacks));
        	}
        } finally {
        	serverSocket.close();
        }
    }
	

	@Override
	public synchronized void signin(String name, String password) throws DuplicateName, RemoteException {

		if(Users.containsKey(name))
			throw new DuplicateName();
		Users.put(name, new UserData(password, "DISCONNECTED"));
	}

	@Override
	public synchronized void login(String name, String password, Object callback) throws NameNotFoundException, RemoteException, NoPermissionException, DuplicateName {
		
		if(!Users.containsKey(name))
			throw new NameNotFoundException();
		if((Users.get(name).getPassword()).equals(password)){
			if((Users.get(name).getStatus()).equals("DISCONNECTED")){
				Users.get(name).setStatus("CONNECTED");
				callbacks.add((Object) callback);
				((UserIF) callback).update(statusGames());
			}
			else
				throw new DuplicateName();
		}
		else
			throw new NoPermissionException();
		
	}

	@Override
	public synchronized void logout(String name, Object callback) throws RemoteException {
		if(Users.containsKey(name)){
			Users.get(name).setStatus("DISCONNECTED");
			callbacks.remove((Object) callback);
		}
	}
	
	public static String statusGames(){
		Iterator<String> iterator = Games.keySet().iterator();
		String ris = "";  
		while (iterator.hasNext()) {
		   String key = iterator.next().toString();
		   String value = Games.get(key).stampGames();
		   ris = " *** " + ris +(key + " " + value) + "\r\n";
		}
		if (ris.equals(""))
			return " *** Nessuna partita aperta al momento";
		return ris;
	}
}
